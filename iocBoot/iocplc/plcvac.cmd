#!../../bin/linux-x86_64/plc

## You may have to change plc to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/plc.dbd"
plc_registerRecordDeviceDriver pdbbase

# Use the following commands for TCP/IP
#drvAsynIPPortConfigure(const char *portName,
#                       const char *hostInfo,
#                       unsigned int priority,
#                       int noAutoConnect,
#                       int noProcessEos);
# This line is for Modbus TCP
#drvAsynIPPortConfigure("plcvac","192.168.10.10:502",0,0,1)
drvAsynIPPortConfigure("plcvac","$(DEVIP):$(DEVPORT)",0,0,1)
#drvAsynIPPortConfigure("plcvac","127.0.0.1:50123",0,0,1)

#modbusInterposeConfig(const char *portName,
#                      modbusLinkType linkType,
#                      int timeoutMsec, 
#                      int writeDelayMsec)
# This line is for Modbus TCP
modbusInterposeConfig("plcvac",0,2000,0)

# Word access at Modbus address 0
# Access 57 words as outputs.  
# Function code=223
# Default data type unsigned integer.
# drvModbusAsynConfigure("portName", "tcpPortName", slaveAddress, modbusFunction, modbusStartAddress, modbusLength, dataType, pollMsec, "plcType")
drvModbusAsynConfigure(  "Out_Word",      "plcvac",            0,            223,                  0,           29,        0,      100, "PhC_AXC_1050")

# Bit access at Modbus address 57
# Access 4 words for bit outputs.
# Function code=6
# drvModbusAsynConfigure("portName", "tcpPortName", slaveAddress, modbusFunction, modbusStartAddress, modbusLength, dataType, pollMsec,      "plcType")
drvModbusAsynConfigure(  "Out_Bits",      "plcvac",            0,              6,                 29,           21,        0,      100, "PhC_AXC_1050")

# Word access at Modbus address 39
# Access 59 words as inputs.  
# Function code=123
# default data type unsigned integer.
# drvModbusAsynConfigure("portName", "tcpPortName", slaveAddress, modbusFunction, modbusStartAddress, modbusLength, dataType, pollMsec, "plcType")
drvModbusAsynConfigure(   "In_Word",      "plcvac",            0,            123,                 50,           70,        0,      500, "PhC_AXC_1050")


## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadTemplate("plc_vac.substitutions")

## Autosave Settings
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK, DEV=PLCVAC")

## Start any sequence programs
#seq sncxxx,"user=epics"
